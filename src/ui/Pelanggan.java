/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import database.DatabaseConnection;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rizky
 */
public class Pelanggan extends javax.swing.JInternalFrame {

    /**
     * Creates new form Pelanggan
     */
    DefaultTableModel dtm;
    int row = 0;
    ArrayList kdtarifs;
    Connection koneksi;

    public Pelanggan() {
        initComponents();
        koneksi = DatabaseConnection.getKoneksi();
        setEditOff();
        loadDataCombobox();
        showData(null);
    }

    private void setEditOff() {
        input_id_pelanggan.setEnabled(false);
        input_nama.setEnabled(false);
        input_no_meter.setEnabled(false);
        input_alamat.setEnabled(false);
        cb_kode_tarif.setEnabled(false);
        table.clearSelection();
        row = 0;
        btnDelete.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnCancel.setEnabled(false);
        btnSave.setEnabled(false);
    }

    private void setEditOn(String act) {
        input_id_pelanggan.setEnabled(true);
        input_nama.setEnabled(true);
        input_no_meter.setEnabled(true);
        input_alamat.setEnabled(true);
        cb_kode_tarif.setEnabled(true);
        if (act.equals("tabel")) {
            btnDelete.setEnabled(true);
            btnUpdate.setEnabled(true);
            btnCancel.setEnabled(true);
            btnSave.setEnabled(false);
        }
        if (act.equals("add")) {
            btnSave.setEnabled(true);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
        }

    }

    private void clearForm() {
        table.clearSelection();
        input_id_pelanggan.setText("");
        input_nama.setText("");
        input_no_meter.setText("");
        input_alamat.setText("");
        cb_kode_tarif.setSelectedIndex(0);
    }

    private void getDataFromTable() {
        row = table.getSelectedRow();
        input_id_pelanggan.setText(dtm.getValueAt(row, 0).toString());
        input_nama.setText(dtm.getValueAt(row, 1).toString());
        input_no_meter.setText(dtm.getValueAt(row,3).toString());
        input_alamat.setText(dtm.getValueAt(row, 2).toString());
        cb_kode_tarif.setSelectedItem(dtm.getValueAt(row, 4));
        setEditOn("tabel");
    }

    private void loadDataCombobox() {
        cb_kode_tarif.removeAllItems();
        cb_kode_tarif.addItem("");
        try {
            String query = "SELECT * FROM t_tarif";
            Statement stmt = koneksi.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                cb_kode_tarif.addItem(rs.getString("kd_tarif"));
            }

            rs.last();
            int jumlahdata = rs.getRow();
            rs.first();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showData(String cari) {
        String[] kolom = {"ID Pelanggan", "Nama", "Alamat", "No Meter", "Kode Tarif"};
        dtm = new DefaultTableModel(null, kolom);
        try {
            Statement stmt = koneksi.createStatement();
            String query = null;
            if (cari != null){
                query = "SELECT * FROM t_pelanggan WHERE nama like"
                    + "'%"+input_cari.getText()+"%'";
            }
            else {
                query = "SELECT * FROM t_pelanggan";
            }
            System.out.println(query);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String id = rs.getString("id_pelanggan");
                String no = rs.getString("no_meter");
                String nama = rs.getString("nama");
                String alamat = rs.getString("alamat");
                String kd = rs.getString("kd_tarif");

                dtm.addRow(new String[]{id, nama, alamat, no, kd});
            }
            table.setModel(dtm);
        } catch (SQLException e) {
            System.out.println("" + e);
        }
    }

    private void insertData() {
        String Id = input_id_pelanggan.getText().toString();
        String NoMeter = input_no_meter.getText().toString();
        String NamaPel = input_nama.getText().toString();
        String AlamatPel = input_alamat.getText().toString();
        String KodeTarif = cb_kode_tarif.getSelectedItem().toString();

        try {
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO t_pelanggan VALUES('" + Id + "', '" + NamaPel + "', "
                    + "'" + NoMeter + "' , '" + AlamatPel + "', '" + KodeTarif + "') ";
            System.out.println("" + query);
            int success = stmt.executeUpdate(query);
            dtm.getDataVector().removeAllElements();
            if (success == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Pada Database");
        }
    }

    private void editData() {
        String Id = input_id_pelanggan.getText().toString();
        String NoMeter = input_no_meter.getText().toString();
        String NamaPel = input_nama.getText().toString();
        String AlamatPel = input_alamat.getText().toString();
        String KodeTarif = cb_kode_tarif.getSelectedItem().toString();

        try {
            Statement stmt = koneksi.createStatement();
            String sql = "UPDATE t_pelanggan SET "
                    + "id_pelanggan = '" + Id + "', "
                    + "no_meter = '" + NoMeter + "', "
                    + "nama = '" + NamaPel + "', "
                    + "alamat = '" + AlamatPel + "', "
                    + "kd_tarif = '" + KodeTarif + "'"
                    + "WHERE id_pelanggan = '" + Id + "' ";
            int berhasil = stmt.executeUpdate(sql);
            System.out.println(sql);
            dtm.getDataVector().removeAllElements();
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Diupdate");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void deleteData() {
        String deletedId = table.getValueAt(row, 0).toString();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "DELETE FROM t_pelanggan WHERE id_pelanggan = '" + deletedId + "'; ";
            int berhasil = stmt.executeUpdate(query);
            System.out.println(query);
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                dtm.getDataVector().removeAllElements();
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblNamaBarang = new javax.swing.JLabel();
        input_id_pelanggan = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }};
            lblNamaBarang1 = new javax.swing.JLabel();
            input_nama = new javax.swing.JTextField();
            lblNamaBarang2 = new javax.swing.JLabel();
            input_no_meter = new javax.swing.JTextField();
            lblNamaBarang3 = new javax.swing.JLabel();
            jScrollPane2 = new javax.swing.JScrollPane();
            input_alamat = new javax.swing.JTextArea();
            lblNamaBarang4 = new javax.swing.JLabel();
            cb_kode_tarif = new javax.swing.JComboBox<>();
            btnAddNew1 = new javax.swing.JButton();
            btnSave = new javax.swing.JButton();
            btnUpdate = new javax.swing.JButton();
            btnCancel = new javax.swing.JButton();
            btnDelete = new javax.swing.JButton();
            btnClose = new javax.swing.JButton();
            input_cari = new javax.swing.JTextField();
            lblNamaBarang5 = new javax.swing.JLabel();

            jLabel1.setFont(new java.awt.Font("Fira Sans Semi-Light", 1, 14)); // NOI18N
            jLabel1.setText("Data Pelanggan");

            lblNamaBarang.setText("ID Pelanggan");

            table.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null, null},
                    {null, null, null, null, null},
                    {null, null, null, null, null},
                    {null, null, null, null, null}
                },
                new String [] {
                    "ID Pelanggan", "Nama", "Alamat", "No Meter", "Kode Tarif"
                }
            ) {
                boolean[] canEdit = new boolean [] {
                    false, true, false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });
            table.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tableMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(table);
            if (table.getColumnModel().getColumnCount() > 0) {
                table.getColumnModel().getColumn(0).setResizable(false);
                table.getColumnModel().getColumn(1).setResizable(false);
                table.getColumnModel().getColumn(2).setResizable(false);
                table.getColumnModel().getColumn(3).setResizable(false);
                table.getColumnModel().getColumn(4).setResizable(false);
            }

            lblNamaBarang1.setText("Nama");

            input_nama.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    input_namaActionPerformed(evt);
                }
            });

            lblNamaBarang2.setText("No Meter");

            input_no_meter.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    input_no_meterActionPerformed(evt);
                }
            });

            lblNamaBarang3.setText("Alamat");

            input_alamat.setColumns(20);
            input_alamat.setRows(5);
            jScrollPane2.setViewportView(input_alamat);

            lblNamaBarang4.setText("Kode Tarif");

            cb_kode_tarif.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

            btnAddNew1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/add.png"))); // NOI18N
            btnAddNew1.setText("Tambah");
            btnAddNew1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnAddNew1ActionPerformed(evt);
                }
            });

            btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/save.png"))); // NOI18N
            btnSave.setText("Simpan");
            btnSave.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSaveActionPerformed(evt);
                }
            });

            btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/refresh.png"))); // NOI18N
            btnUpdate.setText("Perbaharui");
            btnUpdate.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnUpdateActionPerformed(evt);
                }
            });

            btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/cancel.png"))); // NOI18N
            btnCancel.setText("Batalkan");
            btnCancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
                }
            });

            btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/remove.png"))); // NOI18N
            btnDelete.setText("Hapus");
            btnDelete.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDeleteActionPerformed(evt);
                }
            });

            btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/close.png"))); // NOI18N
            btnClose.setText("Tutup");
            btnClose.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCloseActionPerformed(evt);
                }
            });

            input_cari.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    input_cariKeyPressed(evt);
                }
            });

            lblNamaBarang5.setText("Cari");

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                    .addComponent(lblNamaBarang3)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(lblNamaBarang2)
                                                    .addGap(39, 39, 39)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblNamaBarang4)
                                                .addGap(35, 35, 35)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                                            .addComponent(cb_kode_tarif, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(input_no_meter, javax.swing.GroupLayout.Alignment.TRAILING)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblNamaBarang)
                                            .addComponent(lblNamaBarang1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(input_nama, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                                            .addComponent(input_id_pelanggan)))))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAddNew1, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnUpdate))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(9, 9, 9)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang)
                                .addComponent(input_id_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(input_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang2)
                                .addComponent(input_no_meter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblNamaBarang3)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(10, 10, 10)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang4)
                                .addComponent(cb_kode_tarif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAddNew1)
                                .addComponent(btnSave)
                                .addComponent(btnUpdate))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnDelete)
                                .addComponent(btnCancel)
                                .addComponent(btnClose))
                            .addGap(33, 33, 33))))
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (evt.getClickCount() == 1) {
            getDataFromTable();
        }
    }//GEN-LAST:event_tableMouseClicked

    private void input_namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_namaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_namaActionPerformed

    private void input_no_meterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_no_meterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_no_meterActionPerformed

    private void btnAddNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNew1ActionPerformed
        clearForm();
        input_id_pelanggan.requestFocus();
        setEditOn("add");
    }//GEN-LAST:event_btnAddNew1ActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        insertData();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        setEditOff();
        clearForm();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        deleteData();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        editData();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void input_cariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_input_cariKeyPressed
        showData(input_cari.getText());
    }//GEN-LAST:event_input_cariKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNew1;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cb_kode_tarif;
    private javax.swing.JTextArea input_alamat;
    private javax.swing.JTextField input_cari;
    private javax.swing.JTextField input_id_pelanggan;
    private javax.swing.JTextField input_nama;
    private javax.swing.JTextField input_no_meter;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblNamaBarang;
    private javax.swing.JLabel lblNamaBarang1;
    private javax.swing.JLabel lblNamaBarang2;
    private javax.swing.JLabel lblNamaBarang3;
    private javax.swing.JLabel lblNamaBarang4;
    private javax.swing.JLabel lblNamaBarang5;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
