/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import database.DatabaseConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rizky
 */
public class Tagihan extends javax.swing.JInternalFrame {

    /**
     * Creates new form Tagihan
     */
    DefaultTableModel dtm;
    int row = 0;
    ArrayList idpelanggans, tarifs, idtagihans, dendas;
    int tarif;
    String idtagihan, nama, total;
    Connection koneksi;

    public Tagihan() {
        initComponents();
        koneksi = DatabaseConnection.getKoneksi();
        idpelanggans = new ArrayList();
        tarifs = new ArrayList();
        idtagihans = new ArrayList();
        dendas = new ArrayList();
        setEditOff();
        showData(null);
        loadDataCombobox();
    }

    private void setEditOff() {
        input_bulan.setEnabled(false);
        input_tahun.setEnabled(false);
        input_pemakaian.setEnabled(false);
        cb_pelanggan.setEnabled(false);
        table.clearSelection();
        row = 0;
        btnDelete.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnCancel.setEnabled(false);
        btnSave.setEnabled(false);
        btnBayar.setEnabled(false);
    }

    private void setEditOn(String act) {
        input_bulan.setEnabled(true);
        input_tahun.setEnabled(true);
        input_pemakaian.setEnabled(true);
        cb_pelanggan.setEnabled(true);
        if (act.equals("tabel")) {
            btnDelete.setEnabled(true);
            btnUpdate.setEnabled(true);
            btnCancel.setEnabled(true);
            btnSave.setEnabled(false);
            cb_pelanggan.setEnabled(false);
        }
        if (act.equals("add")) {
            btnSave.setEnabled(true);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
        }

    }

    private void clearForm() {
        table.clearSelection();
        input_bulan.setText("");
        input_pemakaian.setText("");
        input_tahun.setText("");
        txt_total.setText("");
        txt_status.setText("");
        cb_pelanggan.setSelectedIndex(0);
    }

    private void getDataFromTable() {
        row = table.getSelectedRow();
        idtagihan = idtagihans.get(table.getSelectedRow()).toString();
        nama = dtm.getValueAt(row, 1).toString();
        input_bulan.setText(dtm.getValueAt(row, 2).toString());
        input_pemakaian.setText(dtm.getValueAt(row, 4).toString());
        txt_total.setText(dtm.getValueAt(row, 5).toString());
        input_tahun.setText(dtm.getValueAt(row, 3).toString());
        cb_pelanggan.setSelectedItem(dtm.getValueAt(row, 0).toString());
        txt_status.setText(dtm.getValueAt(row,6).toString());
        System.out.println(dtm.getValueAt(row, 0).toString());
        if (txt_status.getText().equals("Belum Lunas")){
            if (!"Admin".equals(Login.hak)){
                btnBayar.setEnabled(true);
            }
            else {
                btnBayar.setEnabled(false);
            }
        }
        else {
            btnBayar.setEnabled(false);
        }
        setEditOn("tabel");
    }

    private void loadDataCombobox() {
        cb_pelanggan.removeAllItems();
        cb_pelanggan.addItem("");
        try {
            String query = "SELECT t_pelanggan.nama, t_pelanggan.id_pelanggan, "
                    + "t_pelanggan.kd_tarif, t_tarif.tarif, t_tarif.denda FROM "
                    + "t_pelanggan INNER JOIN t_tarif ON "
                    + "t_pelanggan.kd_tarif = t_tarif.kd_tarif";
            Statement stmt = koneksi.createStatement();
            System.out.println(query);
            ResultSet rs = stmt.executeQuery(query);
            System.out.println(query);
            while (rs.next()) {
                tarifs.add(rs.getInt("tarif"));
                dendas.add(rs.getInt("denda"));
                cb_pelanggan.addItem(rs.getString("id_pelanggan"));
//                if (idpelanggans.size() != 0){
//                    if (!idpelanggans.contains(rs.getString("id_pelanggan"))){
//                        System.out.println(idpelanggans.get(i)+"-"+rs.getString("id_pelanggan"));
//                        cb_pelanggan.addItem(rs.getString("id_pelanggan"));
//                    }
//                }
//                else if (idpelanggans.size() == 0){
//                    cb_pelanggan.addItem(rs.getString("id_pelanggan"));
//                }
            }

            rs.last();
            int jumlahdata = rs.getRow();
            rs.first();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showData(String cari) {
        String[] kolom = {"ID Pelanggan", "Nama", "Bulan", "Tahun", "Pemakaian",
            "Total Tagihan", "Status"};
        dtm = new DefaultTableModel(null, kolom);
        try {
            Statement stmt = koneksi.createStatement();
            String query = null;
            if (cari != null) {
                query = "SELECT t_pelanggan.id_pelanggan, t_pelanggan.nama, "
                        + "t_tarif.tarif, t_tagihan.bulan, t_tagihan.id_tagihan, "
                        + "t_tagihan.tahun, t_tagihan.pemakaian, "
                        + "t_tagihan.total_tagihan, t_tagihan.status FROM t_tagihan INNER JOIN "
                        + "t_pelanggan ON t_tagihan.id_pelanggan = "
                        + "t_pelanggan.id_pelanggan INNER JOIN t_tarif ON "
                        + "t_pelanggan.kd_tarif = t_tarif.kd_tarif WHERE nama like"
                        + "'%" + input_cari.getText() + "%'";
            } else {
                query = "SELECT t_pelanggan.id_pelanggan, t_pelanggan.nama, "
                        + "t_tarif.tarif, t_tagihan.bulan, t_tagihan.id_tagihan, "
                        + "t_tagihan.tahun, t_tagihan.pemakaian, "
                        + "t_tagihan.total_tagihan, t_tagihan.status FROM t_tagihan INNER JOIN "
                        + "t_pelanggan ON t_tagihan.id_pelanggan = "
                        + "t_pelanggan.id_pelanggan INNER JOIN t_tarif ON "
                        + "t_pelanggan.kd_tarif = t_tarif.kd_tarif";
            }
            System.out.println(query);
            ResultSet rs = stmt.executeQuery(query);

//            idpelanggans.clear();
            idtagihans.clear();
            while (rs.next()) {
                idtagihans.add(rs.getString("id_tagihan"));
                idpelanggans.add(rs.getString("id_pelanggan"));
                String id = rs.getString("id_pelanggan");
                String nama = rs.getString("nama");
                String bulan = rs.getString("bulan");
                String status = rs.getString("status");
                int tahun = rs.getInt("tahun");
                int pemakaian = rs.getInt("pemakaian");
                int total = rs.getInt("total_tagihan");

                dtm.addRow(new String[]{id, nama, bulan, tahun + "", pemakaian + "", total + "",status});
            }
            table.setModel(dtm);
//            loadDataCombobox();
        } catch (SQLException e) {
            System.out.println("" + e);
        }
    }

    private void insertData() {
        String id = String.valueOf(System.currentTimeMillis() / 1000L);
        String bulan = input_bulan.getText().toString();
        int tahun = Integer.parseInt(input_tahun.getText().toString());
        int pemakaian = Integer.parseInt(input_pemakaian.getText().toString());
        String id_pelanggan = cb_pelanggan.getSelectedItem().toString();
        int total = pemakaian
                * Integer.parseInt(tarifs.get(cb_pelanggan.getSelectedIndex()-1).toString());

        if (checkData(id_pelanggan, bulan, tahun)){
        
        try {
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO t_tagihan VALUES('" + id + "', '" + id_pelanggan + "', "
                    + "'" + pemakaian + "' , '" + total + "', '" + bulan + "'"
                    + ", '" + tahun + "', 'Belum Lunas','0') ";
            System.out.println("" + query);
            int success = stmt.executeUpdate(query);
            dtm.getDataVector().removeAllElements();
            if (success == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Pada Database");
        }
        }
    }

    private void editData() {
        String id = String.valueOf(System.currentTimeMillis() / 1000L);
        String bulan = input_bulan.getText().toString();
        int tahun = Integer.parseInt(input_tahun.getText().toString());
        int pemakaian = Integer.parseInt(input_pemakaian.getText().toString());
        String id_pelanggan = cb_pelanggan.getSelectedItem().toString();
        int total = pemakaian
                * Integer.parseInt(tarifs.get(cb_pelanggan.getSelectedIndex()-1).toString());

        try {
            Statement stmt = koneksi.createStatement();
            String sql = "UPDATE t_tagihan SET "
                    + "pemakaian = '" + pemakaian + "', "
                    + "total_tagihan = '" + total + "', "
                    + "tahun = '" + tahun + "', "
                    + "bulan = '" + bulan + "'"
                    + "WHERE id_tagihan = '" + idtagihan + "' ";
            int berhasil = stmt.executeUpdate(sql);
            System.out.println(sql);
            dtm.getDataVector().removeAllElements();
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Diupdate");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean checkData(String id, String bulan, int tahun) {
        boolean status = true;
        try {
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM t_tagihan WHERE id_pelanggan = '" + id + "'"
                    + "AND bulan = '" + bulan + "' AND tahun = '" + tahun + "'";
            ResultSet rs = stmt.executeQuery(query);
            if (rs.first()) {
               if (!rs.getString("id_tagihan").isEmpty()){
                   JOptionPane.showMessageDialog(null, "Data sudah ada");
                   status = false;
               }
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return status;
    }

    private void deleteData() {
//        String deletedId = table.getValueAt(row, 0).toString();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "DELETE FROM t_tagihan WHERE id_tagihan = '" + idtagihan + "'; ";
            int berhasil = stmt.executeUpdate(query);
            System.out.println(query);
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                dtm.getDataVector().removeAllElements();
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNamaBarang = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }};
            btnAddNew1 = new javax.swing.JButton();
            btnSave = new javax.swing.JButton();
            lblNamaBarang1 = new javax.swing.JLabel();
            btnUpdate = new javax.swing.JButton();
            input_bulan = new javax.swing.JTextField();
            btnCancel = new javax.swing.JButton();
            lblNamaBarang2 = new javax.swing.JLabel();
            btnDelete = new javax.swing.JButton();
            input_tahun = new javax.swing.JTextField();
            btnClose = new javax.swing.JButton();
            input_cari = new javax.swing.JTextField();
            lblNamaBarang5 = new javax.swing.JLabel();
            jLabel1 = new javax.swing.JLabel();
            cb_pelanggan = new javax.swing.JComboBox<>();
            lblNamaBarang6 = new javax.swing.JLabel();
            input_pemakaian = new javax.swing.JFormattedTextField();
            lblNamaBarang7 = new javax.swing.JLabel();
            txt_total = new javax.swing.JLabel();
            btnBayar = new javax.swing.JButton();
            lblNamaBarang8 = new javax.swing.JLabel();
            txt_status = new javax.swing.JLabel();

            lblNamaBarang.setText("ID Pelanggan");

            table.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null, null, null},
                    {null, null, null, null, null, null},
                    {null, null, null, null, null, null},
                    {null, null, null, null, null, null}
                },
                new String [] {
                    "ID Pelanggan", "Nama", "Bulan", "Tahun", "Pemakaian", "Total Tagihan"
                }
            ) {
                boolean[] canEdit = new boolean [] {
                    false, false, false, false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });
            table.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tableMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(table);
            if (table.getColumnModel().getColumnCount() > 0) {
                table.getColumnModel().getColumn(0).setResizable(false);
                table.getColumnModel().getColumn(1).setResizable(false);
                table.getColumnModel().getColumn(2).setResizable(false);
                table.getColumnModel().getColumn(3).setResizable(false);
                table.getColumnModel().getColumn(4).setResizable(false);
                table.getColumnModel().getColumn(5).setResizable(false);
            }

            btnAddNew1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/add.png"))); // NOI18N
            btnAddNew1.setText("Tambah");
            btnAddNew1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnAddNew1ActionPerformed(evt);
                }
            });

            btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/save.png"))); // NOI18N
            btnSave.setText("Simpan");
            btnSave.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSaveActionPerformed(evt);
                }
            });

            lblNamaBarang1.setText("Bulan");

            btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/refresh.png"))); // NOI18N
            btnUpdate.setText("Perbaharui");
            btnUpdate.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnUpdateActionPerformed(evt);
                }
            });

            input_bulan.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    input_bulanActionPerformed(evt);
                }
            });

            btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/cancel.png"))); // NOI18N
            btnCancel.setText("Batalkan");
            btnCancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
                }
            });

            lblNamaBarang2.setText("Tahun");

            btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/remove.png"))); // NOI18N
            btnDelete.setText("Hapus");
            btnDelete.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDeleteActionPerformed(evt);
                }
            });

            input_tahun.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    input_tahunActionPerformed(evt);
                }
            });

            btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/close.png"))); // NOI18N
            btnClose.setText("Tutup");
            btnClose.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCloseActionPerformed(evt);
                }
            });

            input_cari.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    input_cariKeyPressed(evt);
                }
            });

            lblNamaBarang5.setText("Cari");

            jLabel1.setFont(new java.awt.Font("Fira Sans Semi-Light", 1, 14)); // NOI18N
            jLabel1.setText("Data Tagihan");

            cb_pelanggan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
            cb_pelanggan.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
                public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                }
                public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                    cb_pelangganPopupMenuWillBecomeInvisible(evt);
                }
                public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
                }
            });

            lblNamaBarang6.setText("Pemakaian");

            input_pemakaian.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
            input_pemakaian.addFocusListener(new java.awt.event.FocusAdapter() {
                public void focusLost(java.awt.event.FocusEvent evt) {
                    input_pemakaianFocusLost(evt);
                }
            });

            lblNamaBarang7.setText("Total Tagihan");

            txt_total.setText("Pemakaian");

            btnBayar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/close.png"))); // NOI18N
            btnBayar.setText("Bayar");
            btnBayar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnBayarActionPerformed(evt);
                }
            });

            lblNamaBarang8.setText("Status");

            txt_status.setText("Pemakaian");

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblNamaBarang)
                                        .addComponent(lblNamaBarang1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblNamaBarang2)
                                        .addComponent(lblNamaBarang6))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(input_bulan, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cb_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(input_pemakaian, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(input_tahun, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE))
                                        .addComponent(txt_total)
                                        .addComponent(txt_status))))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblNamaBarang7)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAddNew1, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnBayar, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                                        .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btnUpdate)
                                            .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblNamaBarang8)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 799, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(3, 3, 3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang)
                                .addComponent(cb_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(input_bulan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang2)
                                .addComponent(input_tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(input_pemakaian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang6))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang7)
                                .addComponent(txt_total))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang8)
                                .addComponent(txt_status))
                            .addGap(68, 68, 68)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAddNew1)
                                .addComponent(btnSave)
                                .addComponent(btnUpdate))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnDelete)
                                .addComponent(btnCancel)
                                .addComponent(btnClose))
                            .addGap(18, 18, 18)
                            .addComponent(btnBayar)
                            .addGap(62, 62, 62))))
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (evt.getClickCount() == 1) {
            getDataFromTable();
        }
    }//GEN-LAST:event_tableMouseClicked

    private void btnAddNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNew1ActionPerformed
        clearForm();
//        loadDataCombobox();
        input_bulan.requestFocus();
        setEditOn("add");
    }//GEN-LAST:event_btnAddNew1ActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        insertData();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        editData();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void input_bulanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_bulanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_bulanActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        setEditOff();
        clearForm();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        deleteData();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void input_tahunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_tahunActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_tahunActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void input_cariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_input_cariKeyPressed
        showData(input_cari.getText());
    }//GEN-LAST:event_input_cariKeyPressed

    private void input_pemakaianFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_input_pemakaianFocusLost
        int pemakaian = Integer.parseInt(input_pemakaian.getText())
                * Integer.parseInt(tarifs.get(cb_pelanggan.getSelectedIndex()-1).toString());
        txt_total.setText(pemakaian + "");
    }//GEN-LAST:event_input_pemakaianFocusLost

    private void cb_pelangganPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_cb_pelangganPopupMenuWillBecomeInvisible

    }//GEN-LAST:event_cb_pelangganPopupMenuWillBecomeInvisible

    private void btnBayarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBayarActionPerformed
        if(Dashboard.dekstop.getComponentCount()>1){
            Dashboard.dekstop.remove(1);
        }
        PembayaranForm frm = new PembayaranForm(
                new DataTransaksi(idtagihan, cb_pelanggan.getSelectedItem().toString()
                        , nama,txt_total.getText(), dendas.get(cb_pelanggan.getSelectedIndex()-1).toString()));
        Dashboard.dekstop.add(frm);
        frm.setVisible(true);
        Dashboard.dekstop.updateUI();
    }//GEN-LAST:event_btnBayarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNew1;
    private javax.swing.JButton btnBayar;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cb_pelanggan;
    private javax.swing.JTextField input_bulan;
    private javax.swing.JTextField input_cari;
    private javax.swing.JFormattedTextField input_pemakaian;
    private javax.swing.JTextField input_tahun;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNamaBarang;
    private javax.swing.JLabel lblNamaBarang1;
    private javax.swing.JLabel lblNamaBarang2;
    private javax.swing.JLabel lblNamaBarang5;
    private javax.swing.JLabel lblNamaBarang6;
    private javax.swing.JLabel lblNamaBarang7;
    private javax.swing.JLabel lblNamaBarang8;
    private javax.swing.JTable table;
    private javax.swing.JLabel txt_status;
    private javax.swing.JLabel txt_total;
    // End of variables declaration//GEN-END:variables
}
