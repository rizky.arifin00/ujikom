/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import database.DatabaseConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rizky
 */
public class PembayaranForm extends javax.swing.JInternalFrame {

    /**
     * Creates new form PembayaranForm
     */
    Connection koneksi;

    DataTransaksi data;
    DefaultTableModel dtm;

    String idpel, idtag;
    int subtotal, denda;
    
    public PembayaranForm(DataTransaksi data) {
        initComponents();

        koneksi = DatabaseConnection.getKoneksi();

        this.data = data;

        idpel = data.getId_pelanggan();
        idtag = data.getId_tagihan();

        txt_id_pelanggan.setText(data.getId_pelanggan());
        txt_nama.setText(data.getNama_pelanggan());
        txt_jmlh_tagihan.setText(data.getJumlah_tagihan());
        input_status.setEnabled(false);

//        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd");
        Date now = new Date();
        String tgl = sdfDate.format(now);

        if (Integer.parseInt(tgl) >= 20) {
            int telat = Integer.parseInt(tgl) - 20;
            input_status.setText("Telat Bayar");
            txt_denda.setText((Integer.parseInt(data.getDenda()) * telat) + "");
            denda =(Integer.parseInt(data.getDenda()) * telat); 
        } else {
            input_status.setText("Tepat Waktu");
            txt_denda.setText("0");
            denda = 0;
        }
        
        showData();
    }

    private void insertData() {
        int biaya = Integer.parseInt(input_biaya.getText());
        int bayar = Integer.parseInt(input_bayar.getText());
        int kembali = Integer.parseInt(txt_kembali.getText());
        String id = String.valueOf(System.currentTimeMillis() / 1000L);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO `t_pembayaran`"
                    + "VALUES ('" + id + "','" + idtag + "','" + biaya + "',"
                    + "'" + bayar + "','" + dateFormat.format(date) + "',"
                    + "'" + Login.id + "','" + kembali + "');  ";
            System.out.println(query);
            int berhasil = stmt.executeUpdate(query);
            if (berhasil == 1) {
                berhasil=0;
                query = "UPDATE t_tagihan SET status = 'Lunas' WHERE id_tagihan "
                        + "= '"+data.getId_tagihan()+"' ";
                System.out.println(query);
                berhasil = stmt.executeUpdate(query);
                    if(berhasil == 1){
                        JOptionPane.showMessageDialog(null, "Data Berhasil Dimasukkan");
                        showData();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Data gagal dimasukkan");
                    }
                stmt.close();

            } else {
                JOptionPane.showMessageDialog(null, "Data gagal dimasukkan");
                stmt.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi kesalahan dalam database");
        }
    }

    public void showData() {
        String[] kolom = {"NO", "ID Pelanggan", "Nama Pelanggan",
            "Jumlah Tagihan", "Denda", "Biaya Admin", "Bayar", "Kembali"};
        dtm = new DefaultTableModel(null, kolom);
        try {
            int no = 1;
            Statement stmt = koneksi.createStatement();
            String query = "SELECT t_pelanggan.id_pelanggan, t_pelanggan.nama, "
                    + "t_tagihan.total_tagihan, t_tagihan.denda, "
                    + "t_pembayaran.biaya_admin, t_pembayaran.total_bayar, "
                    + "t_pembayaran.kembalian FROM t_pembayaran INNER JOIN "
                    + "t_tagihan ON t_pembayaran.id_tagihan = "
                    + "t_tagihan.id_tagihan INNER JOIN t_pelanggan ON "
                    + "t_tagihan.id_pelanggan = t_pelanggan.id_pelanggan";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String id = rs.getString("id_pelanggan");
                String nama = rs.getString("nama");
                int tagihan = rs.getInt("total_tagihan");
                int denda = rs.getInt("denda");
                int biaya_admin = rs.getInt("biaya_admin");
                int bayar = rs.getInt("total_Bayar");
                int kembalian = rs.getInt("kembalian");

                dtm.addRow(new String[]{no + "", id, nama, tagihan + "", denda + "",
                    biaya_admin + "", bayar + "", kembalian + ""});
                no++;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        table.setModel(dtm);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNamaBarang1 = new javax.swing.JLabel();
        btnUpdate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblNamaBarang2 = new javax.swing.JLabel();
        btnDelete = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblNamaBarang = new javax.swing.JLabel();
        btnAddNew1 = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        txt_id_pelanggan = new javax.swing.JLabel();
        txt_nama = new javax.swing.JLabel();
        lblNamaBarang7 = new javax.swing.JLabel();
        txt_jmlh_tagihan = new javax.swing.JLabel();
        lblNamaBarang9 = new javax.swing.JLabel();
        lblNamaBarang10 = new javax.swing.JLabel();
        txt_denda = new javax.swing.JLabel();
        lblNamaBarang12 = new javax.swing.JLabel();
        txt_total = new javax.swing.JLabel();
        lblNamaBarang14 = new javax.swing.JLabel();
        txt_kembali = new javax.swing.JLabel();
        lblNamaBarang16 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }};
            input_bayar = new javax.swing.JFormattedTextField();
            input_status = new javax.swing.JFormattedTextField();
            input_biaya = new javax.swing.JFormattedTextField();

            lblNamaBarang1.setText("Nama");

            btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/refresh.png"))); // NOI18N
            btnUpdate.setText("Perbaharui");
            btnUpdate.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnUpdateActionPerformed(evt);
                }
            });

            btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/cancel.png"))); // NOI18N
            btnCancel.setText("Batalkan");
            btnCancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
                }
            });

            lblNamaBarang2.setText("Bayar");

            btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/remove.png"))); // NOI18N
            btnDelete.setText("Hapus");
            btnDelete.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDeleteActionPerformed(evt);
                }
            });

            btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/close.png"))); // NOI18N
            btnClose.setText("Tutup");
            btnClose.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCloseActionPerformed(evt);
                }
            });

            jLabel1.setFont(new java.awt.Font("Fira Sans Semi-Light", 1, 14)); // NOI18N
            jLabel1.setText("Form Pembayaran");

            lblNamaBarang.setText("ID Pelanggan");

            btnAddNew1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/add.png"))); // NOI18N
            btnAddNew1.setText("Tambah");
            btnAddNew1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnAddNew1ActionPerformed(evt);
                }
            });

            btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/save.png"))); // NOI18N
            btnSave.setText("Simpan");
            btnSave.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSaveActionPerformed(evt);
                }
            });

            txt_id_pelanggan.setText("ID Pelanggan");

            txt_nama.setText("ID Pelanggan");

            lblNamaBarang7.setText("Jumlah Tagihan");

            txt_jmlh_tagihan.setText("ID Pelanggan");

            lblNamaBarang9.setText("Status");

            lblNamaBarang10.setText("Denda");

            txt_denda.setText("ID Pelanggan");

            lblNamaBarang12.setText("Sub Total");

            txt_total.setText("ID Pelanggan");

            lblNamaBarang14.setText("Kembali");

            txt_kembali.setText("ID Pelanggan");

            lblNamaBarang16.setText("Biaya Admin");

            table.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "ID Pelanggan", "Nama", "Jumlah Tagihan", "Biaya Admin", "Denda", "Total Bayar", "Kembali"
                }
            ) {
                boolean[] canEdit = new boolean [] {
                    false, false, false, false, false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });
            table.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tableMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(table);

            input_bayar.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
            input_bayar.addFocusListener(new java.awt.event.FocusAdapter() {
                public void focusLost(java.awt.event.FocusEvent evt) {
                    input_bayarFocusLost(evt);
                }
            });

            input_status.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
            input_status.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    input_statusActionPerformed(evt);
                }
            });

            input_biaya.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
            input_biaya.addFocusListener(new java.awt.event.FocusAdapter() {
                public void focusLost(java.awt.event.FocusEvent evt) {
                    input_biayaFocusLost(evt);
                }
            });

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblNamaBarang)
                                        .addComponent(lblNamaBarang1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblNamaBarang7, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblNamaBarang16)
                                        .addComponent(lblNamaBarang9))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(16, 16, 16)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(txt_nama, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(txt_id_pelanggan, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(txt_jmlh_tagihan, javax.swing.GroupLayout.Alignment.TRAILING)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(input_biaya, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(input_status, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(lblNamaBarang10, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txt_denda))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(lblNamaBarang14, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txt_kembali))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblNamaBarang12, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblNamaBarang2))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addComponent(txt_total))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addComponent(input_bayar, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnAddNew1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnUpdate)
                                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(34, 34, 34)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 697, Short.MAX_VALUE)))
                    .addGap(44, 44, 44))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel1)
                    .addGap(15, 15, 15)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang)
                                .addComponent(txt_id_pelanggan))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_nama))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_jmlh_tagihan))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang16)
                                .addComponent(input_biaya, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(input_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang9))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_denda))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang2)
                                .addComponent(input_bayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_total))
                            .addGap(1, 1, 1)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_kembali))
                            .addGap(51, 51, 51)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAddNew1)
                                .addComponent(btnSave)
                                .addComponent(btnUpdate))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnDelete)
                                .addComponent(btnCancel)
                                .addComponent(btnClose))
                            .addGap(105, 105, 105))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed

    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed

    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnAddNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNew1ActionPerformed

    }//GEN-LAST:event_btnAddNew1ActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        insertData();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked

    }//GEN-LAST:event_tableMouseClicked

    private void input_statusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_statusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_statusActionPerformed

    private void input_biayaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_input_biayaFocusLost
        if (!input_biaya.getText().isEmpty()){
            int biaya = Integer.parseInt(input_biaya.getText());
            subtotal = Integer.parseInt(data.getJumlah_tagihan())+denda+biaya;
            txt_total.setText(subtotal+"");
        }
    }//GEN-LAST:event_input_biayaFocusLost

    private void input_bayarFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_input_bayarFocusLost
        if (!input_bayar.getText().isEmpty()){
            int bayar = Integer.parseInt(input_bayar.getText());
            txt_kembali.setText(bayar-subtotal+"");
        }
    }//GEN-LAST:event_input_bayarFocusLost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNew1;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JFormattedTextField input_bayar;
    private javax.swing.JFormattedTextField input_biaya;
    private javax.swing.JFormattedTextField input_status;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNamaBarang;
    private javax.swing.JLabel lblNamaBarang1;
    private javax.swing.JLabel lblNamaBarang10;
    private javax.swing.JLabel lblNamaBarang12;
    private javax.swing.JLabel lblNamaBarang14;
    private javax.swing.JLabel lblNamaBarang16;
    private javax.swing.JLabel lblNamaBarang2;
    private javax.swing.JLabel lblNamaBarang7;
    private javax.swing.JLabel lblNamaBarang9;
    private javax.swing.JTable table;
    private javax.swing.JLabel txt_denda;
    private javax.swing.JLabel txt_id_pelanggan;
    private javax.swing.JLabel txt_jmlh_tagihan;
    private javax.swing.JLabel txt_kembali;
    private javax.swing.JLabel txt_nama;
    private javax.swing.JLabel txt_total;
    // End of variables declaration//GEN-END:variables
}
