/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import database.DatabaseConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rizky
 */
public class Tarif extends javax.swing.JInternalFrame {

    /**
     * Creates new form Tarif
     */
    DefaultTableModel dtm;
    int row = 0;
    Connection koneksi;
    public Tarif() {
        koneksi = DatabaseConnection.getKoneksi();
        initComponents();
        setEditOff();
        showData(null);
    }
    
    private void setEditOff() {
        input_kode_tarif.setEnabled(false);
        input_daya.setEnabled(false);
        input_tarif.setEnabled(false);
        input_denda.setEnabled(false);
        table.clearSelection();
        row = 0;
        btnDelete.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnCancel.setEnabled(false);
        btnSave.setEnabled(false);
    }

    private void setEditOn(String act) {
        input_kode_tarif.setEnabled(true);
        input_daya.setEnabled(true);
        input_tarif.setEnabled(true);
        input_denda.setEnabled(true);
        if (act.equals("tabel")) {
            btnDelete.setEnabled(true);
            btnUpdate.setEnabled(true);
            btnCancel.setEnabled(true);
            btnSave.setEnabled(false);
        }
        if (act.equals("add")) {
            btnSave.setEnabled(true);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
        }

    }

    private void clearForm() {
        table.clearSelection();
        input_kode_tarif.setText("");
        input_daya.setText("");
        input_tarif.setText("");
        input_denda.setText("");
    }

    private void getDataFromTable() {
        row = table.getSelectedRow();
        input_kode_tarif.setText(dtm.getValueAt(row, 0).toString());
        input_daya.setText(dtm.getValueAt(row, 1).toString());
        input_tarif.setText(dtm.getValueAt(row,2).toString());
        input_denda.setText(dtm.getValueAt(row, 3).toString());
        setEditOn("tabel");
    }

    private void showData(String cari) {
        String[] kolom = {"Kode Tarif", "Daya", "Tarif/Kwh", "Denda"};
        dtm = new DefaultTableModel(null, kolom);
        try {
            Statement stmt = koneksi.createStatement();
            String query = null;
            if (cari != null){
                query = "SELECT * FROM t_tarif WHERE daya like"
                    + "'%"+input_cari.getText()+"%'";
            }
            else {
                query = "SELECT * FROM t_tarif";
            }
            System.out.println(query);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String kode = rs.getString("kd_tarif");
                int daya = rs.getInt("daya");
                int tarif = rs.getInt("tarif");
                int denda = rs.getInt("denda");

                dtm.addRow(new String[]{kode, daya+"", tarif+"", denda+""});
            }
            table.setModel(dtm);
        } catch (SQLException e) {
            System.out.println("" + e);
        }
    }

    private void insertData() {
        String Id = input_kode_tarif.getText().toString();
        String daya = input_daya.getText().toString();
        String tarif = input_tarif.getText().toString();
        String denda = input_denda.getText().toString();

        try {
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO t_tarif VALUES('" + Id + "', '" + daya + "', "
                    + "'" + tarif + "' , '" + denda + "') ";
            System.out.println("" + query);
            int success = stmt.executeUpdate(query);
            dtm.getDataVector().removeAllElements();
            if (success == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Pada Database");
        }
    }

    private void editData() {
     
        String Id = input_kode_tarif.getText().toString();
        int daya = Integer.parseInt(input_daya.getText());
        int tarif = Integer.parseInt(input_tarif.getText());
        int denda = Integer.parseInt(input_denda.getText());

        try {
            Statement stmt = koneksi.createStatement();
            String sql = "UPDATE t_tarif SET "
                    + "kd_tarif = '" + Id + "', "
                    + "daya = '" + daya + "', "
                    + "tarif = '" + tarif + "', "
                    + "denda = '" + denda + "'"
                    + "WHERE kd_tarif = '" + Id + "'";
            int berhasil = stmt.executeUpdate(sql);
            dtm.getDataVector().removeAllElements();
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Diupdate");
            }
            System.out.println(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void deleteData() {
        String deletedId = table.getValueAt(row, 0).toString();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "DELETE FROM t_tarif WHERE kd_tarif = '" + deletedId + "'; ";
            int berhasil = stmt.executeUpdate(query);
            System.out.println(query);
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                dtm.getDataVector().removeAllElements();
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnUpdate = new javax.swing.JButton();
        btnAddNew1 = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblNamaBarang2 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        lblNamaBarang = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();
        input_kode_tarif = new javax.swing.JTextField();
        input_cari = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }};
            lblNamaBarang5 = new javax.swing.JLabel();
            lblNamaBarang1 = new javax.swing.JLabel();
            input_daya = new javax.swing.JFormattedTextField();
            input_tarif = new javax.swing.JFormattedTextField();
            lblNamaBarang6 = new javax.swing.JLabel();
            input_denda = new javax.swing.JFormattedTextField();

            btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/refresh.png"))); // NOI18N
            btnUpdate.setText("Perbaharui");
            btnUpdate.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnUpdateActionPerformed(evt);
                }
            });

            btnAddNew1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/add.png"))); // NOI18N
            btnAddNew1.setText("Tambah");
            btnAddNew1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnAddNew1ActionPerformed(evt);
                }
            });

            btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/cancel.png"))); // NOI18N
            btnCancel.setText("Batalkan");
            btnCancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
                }
            });

            jLabel1.setFont(new java.awt.Font("Fira Sans Semi-Light", 1, 14)); // NOI18N
            jLabel1.setText("Data Tarif");

            lblNamaBarang2.setText("Tarif/Kwh");

            btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/save.png"))); // NOI18N
            btnSave.setText("Simpan");
            btnSave.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSaveActionPerformed(evt);
                }
            });

            btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/remove.png"))); // NOI18N
            btnDelete.setText("Hapus");
            btnDelete.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDeleteActionPerformed(evt);
                }
            });

            lblNamaBarang.setText("Kode Tarif");

            btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/close.png"))); // NOI18N
            btnClose.setText("Tutup");
            btnClose.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCloseActionPerformed(evt);
                }
            });

            input_cari.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    input_cariKeyPressed(evt);
                }
            });

            table.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null}
                },
                new String [] {
                    "Kode Tarif", "Daya", "Tarif/Kwh", "Denda"
                }
            ) {
                boolean[] canEdit = new boolean [] {
                    false, false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });
            table.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tableMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(table);
            if (table.getColumnModel().getColumnCount() > 0) {
                table.getColumnModel().getColumn(0).setResizable(false);
                table.getColumnModel().getColumn(1).setResizable(false);
                table.getColumnModel().getColumn(2).setResizable(false);
                table.getColumnModel().getColumn(3).setResizable(false);
            }

            lblNamaBarang5.setText("Cari");

            lblNamaBarang1.setText("Daya");

            input_daya.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));

            input_tarif.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));

            lblNamaBarang6.setText("Denda");

            input_denda.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAddNew1, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnUpdate))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblNamaBarang2)
                                        .addComponent(lblNamaBarang6))
                                    .addGap(35, 35, 35)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(input_denda, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                                        .addComponent(input_tarif)))
                                .addComponent(jLabel1)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblNamaBarang)
                                        .addComponent(lblNamaBarang1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(31, 31, 31)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(input_kode_tarif, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(input_daya, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(9, 9, 9)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang)
                                .addComponent(input_kode_tarif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(input_daya, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang2)
                                .addComponent(input_tarif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang6)
                                .addComponent(input_denda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(31, 31, 31)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAddNew1)
                                .addComponent(btnSave)
                                .addComponent(btnUpdate))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnDelete)
                                .addComponent(btnCancel)
                                .addComponent(btnClose))
                            .addGap(138, 138, 138))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        editData();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnAddNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNew1ActionPerformed
        clearForm();
        input_kode_tarif.requestFocus();
        setEditOn("add");
    }//GEN-LAST:event_btnAddNew1ActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        setEditOff();
        clearForm();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        insertData();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        deleteData();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void input_cariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_input_cariKeyPressed
        showData(input_cari.getText());
    }//GEN-LAST:event_input_cariKeyPressed

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (evt.getClickCount() == 1) {
            getDataFromTable();
        }
    }//GEN-LAST:event_tableMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNew1;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JTextField input_cari;
    private javax.swing.JFormattedTextField input_daya;
    private javax.swing.JFormattedTextField input_denda;
    private javax.swing.JTextField input_kode_tarif;
    private javax.swing.JFormattedTextField input_tarif;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNamaBarang;
    private javax.swing.JLabel lblNamaBarang1;
    private javax.swing.JLabel lblNamaBarang2;
    private javax.swing.JLabel lblNamaBarang5;
    private javax.swing.JLabel lblNamaBarang6;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
