/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

/**
 *
 * @author rizky
 */
public class DataTransaksi {
    private String id_tagihan;
    private String id_pelanggan;
    private String nama_pelanggan;
    private String jumlah_tagihan;
    private String denda;

    public DataTransaksi(String id_tagihan, String id_pelanggan, String nama_pelanggan, String jumlah_tagihan, String denda) {
        this.id_tagihan = id_tagihan;
        this.id_pelanggan = id_pelanggan;
        this.nama_pelanggan = nama_pelanggan;
        this.jumlah_tagihan = jumlah_tagihan;
        this.denda = denda;
    }

    public String getId_tagihan() {
        return id_tagihan;
    }

    public void setId_tagihan(String id_tagihan) {
        this.id_tagihan = id_tagihan;
    }

    public String getId_pelanggan() {
        return id_pelanggan;
    }

    public void setId_pelanggan(String id_pelanggan) {
        this.id_pelanggan = id_pelanggan;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
    }

    public String getJumlah_tagihan() {
        return jumlah_tagihan;
    }

    public void setJumlah_tagihan(String jumlah_tagihan) {
        this.jumlah_tagihan = jumlah_tagihan;
    }

    public String getDenda() {
        return denda;
    }

    public void setDenda(String denda) {
        this.denda = denda;
    }

    
}
