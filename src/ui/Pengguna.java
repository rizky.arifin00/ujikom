/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import database.DatabaseConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rizky
 */
public class Pengguna extends javax.swing.JInternalFrame {

    /**
     * Creates new form Pengguna
     */
    
    DefaultTableModel dtm;
    int row = 0;
    Connection koneksi;
    
    public Pengguna() {
        initComponents();
        koneksi = DatabaseConnection.getKoneksi();
        setEditOff();
        showData(null);
    }
    
    private void setEditOff() {
        input_id_pengguna.setEnabled(false);
        input_nama.setEnabled(false);
        input_email.setEnabled(false);
        input_no_hp.setEnabled(false);
        input_username.setEnabled(false);
        input_password.setEnabled(false);
        cb_hak_akses.setEnabled(false);
        table.clearSelection();
        row = 0;
        btnDelete.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnCancel.setEnabled(false);
        btnSave.setEnabled(false);
    }

    private void setEditOn(String act) {
        input_id_pengguna.setEnabled(true);
        input_nama.setEnabled(true);
        input_email.setEnabled(true);
        input_no_hp.setEnabled(true);
        input_username.setEnabled(true);
        input_password.setEnabled(true);
        cb_hak_akses.setEnabled(true);
        if (act.equals("tabel")) {
            btnDelete.setEnabled(true);
            btnUpdate.setEnabled(true);
            btnCancel.setEnabled(true);
            btnSave.setEnabled(false);
            input_password.setEnabled(false);
        }
        if (act.equals("add")) {
            btnSave.setEnabled(true);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
        }

    }

    private void clearForm() {
        table.clearSelection();
        input_id_pengguna.setText("");
        input_nama.setText("");
        input_email.setText("");
        input_no_hp.setText("");
        input_username.setText("");
        input_password.setText("");
        cb_hak_akses.setSelectedIndex(0);
    }

    private void getDataFromTable() {
        row = table.getSelectedRow();
        input_id_pengguna.setText(dtm.getValueAt(row, 0).toString());
        input_nama.setText(dtm.getValueAt(row, 1).toString());
        input_no_hp.setText(dtm.getValueAt(row, 2).toString());
        input_email.setText(dtm.getValueAt(row, 3).toString());
        input_username.setText(dtm.getValueAt(row, 4).toString());
        cb_hak_akses.setSelectedItem(dtm.getValueAt(row, 5));
        setEditOn("tabel");
    }
    
    private void showData(String cari) {
        String[] kolom = {"ID Pengguna", "Nama", "No Hp", "Email", "Username", "Hak Akses"};
        dtm = new DefaultTableModel(null, kolom);
        try {
            Statement stmt = koneksi.createStatement();
            String query = null;
            if (cari != null){
                query = "SELECT * FROM t_pengguna WHERE nama like"
                    + "'%"+input_cari.getText()+"%'";
            }
            else {
                query = "SELECT * FROM t_pengguna";
            }
            System.out.println(query);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String id = rs.getString("id_pengguna");
                String no = rs.getString("no_hp");
                String nama = rs.getString("nama");
                String email = rs.getString("email");
                String username = rs.getString("username");
                String hak = rs.getString("hak_akses");

                dtm.addRow(new String[]{id,nama ,no,email, username, hak});
            }
            table.setModel(dtm);
        } catch (SQLException e) {
            System.out.println("" + e);
        }
    }

    private void insertData() {
        String id = input_id_pengguna.getText().toString();
        String nama = input_nama.getText().toString();
        String email = input_email.getText().toString();
        String nohp = input_no_hp.getText().toString();
        String username = input_username.getText().toString();
        String password = new String(input_password.getPassword());
        String hak = cb_hak_akses.getSelectedItem().toString();

        try {
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO t_pengguna VALUES('" + id + "', '" + nama + "', "
                    + "'" + username + "' , SHA1('" + password + "'), '" + email + "'"
                    + ",'" + nohp + "','" + hak + "') ";
            System.out.println("" + query);
            int success = stmt.executeUpdate(query);
            dtm.getDataVector().removeAllElements();
            if (success == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Pada Database");
        }
    }

    private void editData() {
        String id = input_id_pengguna.getText().toString();
        String nama = input_nama.getText().toString();
        String email = input_email.getText().toString();
        String nohp = input_no_hp.getText().toString();
        String username = input_username.getText().toString();
        String password = new String(input_password.getPassword());
        String hak = cb_hak_akses.getSelectedItem().toString();

        try {
            Statement stmt = koneksi.createStatement();
            String sql = "UPDATE t_pengguna SET "
                    + "id_pengguna = '" + id + "', "
                    + "nama = '" + nama + "', "
                    + "email = '" + email + "', "
                    + "no_hp = '" + nohp + "', "
                    + "username = '" + username + "',"
                    + "hak_akses = '" + hak + "'"
                    + "WHERE id_pengguna = '" + id + "' ";
            int berhasil = stmt.executeUpdate(sql);
            System.out.println(sql);
            dtm.getDataVector().removeAllElements();
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Diupdate");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void deleteData() {
        String deletedId = table.getValueAt(row, 0).toString();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "DELETE FROM t_pengguna WHERE id_pengguna = '" + deletedId + "'; ";
            int berhasil = stmt.executeUpdate(query);
            System.out.println(query);
            if (berhasil == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                dtm.getDataVector().removeAllElements();
                showData(null);
                clearForm();
                setEditOff();
            } else {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNamaBarang = new javax.swing.JLabel();
        input_id_pengguna = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }};
            btnAddNew1 = new javax.swing.JButton();
            btnSave = new javax.swing.JButton();
            lblNamaBarang1 = new javax.swing.JLabel();
            btnUpdate = new javax.swing.JButton();
            input_nama = new javax.swing.JTextField();
            btnCancel = new javax.swing.JButton();
            lblNamaBarang2 = new javax.swing.JLabel();
            btnDelete = new javax.swing.JButton();
            input_email = new javax.swing.JTextField();
            btnClose = new javax.swing.JButton();
            lblNamaBarang3 = new javax.swing.JLabel();
            input_cari = new javax.swing.JTextField();
            lblNamaBarang5 = new javax.swing.JLabel();
            jLabel1 = new javax.swing.JLabel();
            lblNamaBarang6 = new javax.swing.JLabel();
            lblNamaBarang7 = new javax.swing.JLabel();
            input_password = new javax.swing.JPasswordField();
            input_username = new javax.swing.JTextField();
            cb_hak_akses = new javax.swing.JComboBox<>();
            lblNamaBarang8 = new javax.swing.JLabel();
            input_no_hp = new javax.swing.JTextField();

            lblNamaBarang.setText("ID Pengguna");

            table.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null, null, null},
                    {null, null, null, null, null, null},
                    {null, null, null, null, null, null},
                    {null, null, null, null, null, null}
                },
                new String [] {
                    "ID Pengguna", "Nama", "No Hp", "Email", "Username", "Hak Akses"
                }
            ) {
                boolean[] canEdit = new boolean [] {
                    false, false, false, false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });
            table.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tableMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(table);
            if (table.getColumnModel().getColumnCount() > 0) {
                table.getColumnModel().getColumn(0).setResizable(false);
                table.getColumnModel().getColumn(1).setResizable(false);
                table.getColumnModel().getColumn(2).setResizable(false);
                table.getColumnModel().getColumn(3).setResizable(false);
                table.getColumnModel().getColumn(4).setResizable(false);
                table.getColumnModel().getColumn(5).setResizable(false);
            }

            btnAddNew1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/add.png"))); // NOI18N
            btnAddNew1.setText("Tambah");
            btnAddNew1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnAddNew1ActionPerformed(evt);
                }
            });

            btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/save.png"))); // NOI18N
            btnSave.setText("Simpan");
            btnSave.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSaveActionPerformed(evt);
                }
            });

            lblNamaBarang1.setText("Nama");

            btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/refresh.png"))); // NOI18N
            btnUpdate.setText("Perbaharui");
            btnUpdate.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnUpdateActionPerformed(evt);
                }
            });

            input_nama.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    input_namaActionPerformed(evt);
                }
            });

            btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/cancel.png"))); // NOI18N
            btnCancel.setText("Batalkan");
            btnCancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
                }
            });

            lblNamaBarang2.setText("Email");

            btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/remove.png"))); // NOI18N
            btnDelete.setText("Hapus");
            btnDelete.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDeleteActionPerformed(evt);
                }
            });

            input_email.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    input_emailActionPerformed(evt);
                }
            });

            btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconpack/close.png"))); // NOI18N
            btnClose.setText("Tutup");
            btnClose.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCloseActionPerformed(evt);
                }
            });

            lblNamaBarang3.setText("No. Hp");

            input_cari.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    input_cariKeyPressed(evt);
                }
            });

            lblNamaBarang5.setText("Cari");

            jLabel1.setFont(new java.awt.Font("Fira Sans Semi-Light", 1, 14)); // NOI18N
            jLabel1.setText("Data Pengguna");

            lblNamaBarang6.setText("Username");

            lblNamaBarang7.setText("Password");

            cb_hak_akses.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Petugas", "Admin" }));

            lblNamaBarang8.setText("Hak Akses");

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAddNew1, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                                .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnClose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblNamaBarang)
                                    .addComponent(lblNamaBarang1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(input_nama, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                                    .addComponent(input_id_pengguna)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblNamaBarang7)
                                    .addComponent(lblNamaBarang6)
                                    .addComponent(lblNamaBarang3)
                                    .addComponent(lblNamaBarang2)
                                    .addComponent(lblNamaBarang8))
                                .addGap(54, 54, 54)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(input_email, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                                    .addComponent(input_password)
                                    .addComponent(input_username)
                                    .addComponent(cb_hak_akses, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(input_no_hp)))))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(77, 77, 77)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 593, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(input_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNamaBarang5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(9, 9, 9)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                            .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang)
                                .addComponent(input_id_pengguna, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(input_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang2)
                                .addComponent(input_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblNamaBarang3)
                                .addComponent(input_no_hp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(input_username, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang6))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(input_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang7))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cb_hak_akses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNamaBarang8))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAddNew1)
                                .addComponent(btnSave)
                                .addComponent(btnUpdate))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnDelete)
                                .addComponent(btnCancel)
                                .addComponent(btnClose))
                            .addGap(33, 33, 33))))
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (evt.getClickCount() == 1) {
            getDataFromTable();
        }
    }//GEN-LAST:event_tableMouseClicked

    private void btnAddNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNew1ActionPerformed
        clearForm();
        input_id_pengguna.requestFocus();
        setEditOn("add");
    }//GEN-LAST:event_btnAddNew1ActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        insertData();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        editData();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void input_namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_namaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_namaActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        setEditOff();
        clearForm();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        deleteData();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void input_emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_emailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_emailActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void input_cariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_input_cariKeyPressed
        showData(input_cari.getText());
    }//GEN-LAST:event_input_cariKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNew1;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cb_hak_akses;
    private javax.swing.JTextField input_cari;
    private javax.swing.JTextField input_email;
    private javax.swing.JTextField input_id_pengguna;
    private javax.swing.JTextField input_nama;
    private javax.swing.JTextField input_no_hp;
    private javax.swing.JPasswordField input_password;
    private javax.swing.JTextField input_username;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNamaBarang;
    private javax.swing.JLabel lblNamaBarang1;
    private javax.swing.JLabel lblNamaBarang2;
    private javax.swing.JLabel lblNamaBarang3;
    private javax.swing.JLabel lblNamaBarang5;
    private javax.swing.JLabel lblNamaBarang6;
    private javax.swing.JLabel lblNamaBarang7;
    private javax.swing.JLabel lblNamaBarang8;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
